package com.common.business.valueobjects.items;

import com.common.business.valueobjects.AbstractVO;
import com.common.business.valueobjects.rating.RatingVO;

public class ItemVO extends AbstractVO{
	private String name;
	private RatingVO rating;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public RatingVO getRating() {
		return rating;
	}
	public void setRating(RatingVO rating) {
		this.rating = rating;
	}
	
	
}
