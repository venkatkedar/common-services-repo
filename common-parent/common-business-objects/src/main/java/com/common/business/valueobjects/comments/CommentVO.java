package com.common.business.valueobjects.comments;

import java.util.Date;
import java.util.List;

import com.common.business.valueobjects.AbstractVO;

public class CommentVO extends AbstractVO{
	private String name;
	private String message;
	private int likes;
	private int dislikes;
	private List<CommentVO> replies;
	private Date lastUpdatedTime;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getLikes() {
		return likes;
	}
	public int incrementLikes() {
		likes++;
		return likes;
	}
	public int decrementLikes() {
		likes--;
		return likes;
	}
	public int getDislikes() {
		return dislikes;
	}
	public int incrementDisLikes() {
		dislikes++;
		return dislikes;
	}
	public int decrementDisLikes() {
		dislikes--;
		return dislikes;
	}
	public List<CommentVO> getReplies() {
		return replies;
	}
	public void setReplies(List<CommentVO> replies) {
		this.replies = replies;
	}
	public Date getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	
	
}
