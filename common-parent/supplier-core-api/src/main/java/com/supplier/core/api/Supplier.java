package com.supplier.core.api;

import com.common.business.models.Commodity;

public interface Supplier<T extends Commodity> {
	void supply(T commodity);
}
