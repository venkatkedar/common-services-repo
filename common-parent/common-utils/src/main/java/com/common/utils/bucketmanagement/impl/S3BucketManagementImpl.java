package com.common.utils.bucketmanagement.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Date;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.common.utils.bucketmanagement.api.BucketManagement;

@Component("s3bucket")
public class S3BucketManagementImpl implements BucketManagement{
	
	private final String ACCESS_KEY_ID="AKIA4ZHFDYURRMYSAKNN";
	private final String ACCESS_SEC_KEY="wmi/OfUPJ4ic46BfmOQhUVeufRgIeDhx3AD3FUTk";
	private final AWSCredentials credentials=new BasicAWSCredentials(ACCESS_KEY_ID, ACCESS_SEC_KEY);
	
	@Override
	public void createBucket(long userId) {
		
		AmazonS3 awsS3=AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_2).build();
		awsS3.createBucket("sample8374634");
	}
	
	public void createFolder() {
		AmazonS3 awsS3=AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_2).build();
		awsS3.putObject(new PutObjectRequest("sample8374634", "abc2/", new ByteArrayInputStream(new byte[0]), new ObjectMetadata()));
	}
	
	public String createFile(String bucketName,File f) {
		AmazonS3 awsS3=AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_2).build();
		Random r=new Random(new Date().getTime());
		String filename="file"+r.nextInt()+".jpg";
		awsS3.putObject(new PutObjectRequest(bucketName, filename, f).withCannedAcl(CannedAccessControlList.PublicRead));//, new ObjectMetadata()));
		return awsS3.getUrl(bucketName, filename).toExternalForm();
	}
	
	public static void main(String[] args) {
		S3BucketManagementImpl s3=new S3BucketManagementImpl();
		String url=s3.createFile("sample8374634",new File("download.jpg"));
		System.out.println(url);
	}
}
