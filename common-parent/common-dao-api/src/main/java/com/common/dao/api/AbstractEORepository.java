package com.common.dao.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.common.entities.AbstractEO;

@NoRepositoryBean
public interface AbstractEORepository<T extends AbstractEO> extends JpaRepository<T,Long> {
	
}
