package com.common.exceptions.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.common.business.exceptions.BadRequestException;
import com.common.business.exceptions.NotFoundException;
import com.common.business.exceptions.ServiceException;
import com.common.exceptions.objects.AppError;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler{

	@ExceptionHandler(value= {ServiceException.class})
	public ResponseEntity<AppError> handleServiceException(ServiceException e){
		return error(e,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(value= {NotFoundException.class})
	public ResponseEntity<AppError> handleNotFoundException(NotFoundException e){
		return error(e,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value= {BadRequestException.class})
	public ResponseEntity<AppError> handleBadRequestException(BadRequestException e){
		return error(e,HttpStatus.BAD_REQUEST);
	}
	
	
	public <T extends ServiceException> ResponseEntity<AppError> error(T e,HttpStatus hs){
		AppError appError=new AppError();
		appError.setMessage(e.getMessage());
		return ResponseEntity.status(hs).body(appError);
	}
}
