package com.common.dao.exceptions;

public class DaoAccessException extends RuntimeException {
	public DaoAccessException() {
		super();
	}
	
	public DaoAccessException(String message) {
		super(message);
	}
}
