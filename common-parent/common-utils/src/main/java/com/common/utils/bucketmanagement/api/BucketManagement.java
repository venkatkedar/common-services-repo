package com.common.utils.bucketmanagement.api;

import java.io.File;

public interface BucketManagement {
	void createBucket(long userId);
	String createFile(String bucketName,File f);
}
