package com.common.business.valueobjects.rating;

import com.common.business.valueobjects.AbstractVO;

public class RatingVO extends AbstractVO{
	private RatingEnum rating;

	public int getRating() {
		return rating.getValue();
	}

	public void setRating(RatingEnum rating) {
		this.rating = rating;
	}
	
}
