package com.consumer.core.api;

import com.common.business.models.Commodity;

public interface Consumer<T extends Commodity> {
	void consume(T commodity);
}
