package com.common.business.exceptions;

public class NotFoundException extends ServiceException {
	public NotFoundException() {
		
	}
	
	public NotFoundException(String message) {
		super(message);
	}
}
