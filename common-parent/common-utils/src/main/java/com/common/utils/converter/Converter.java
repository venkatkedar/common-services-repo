package com.common.utils.converter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.stereotype.Component;

import com.common.business.exceptions.ServiceException;

@Component
public final class Converter {
	public static <U,V> void convertUToV(U u,V v) throws ServiceException{
		try {
			BeanUtils.copyProperties(v, u);
		} catch (IllegalAccessException|InvocationTargetException e) {
			throw new ServiceException(e.getMessage());
		} 
	}
	
	public static <U,V> V convertUToV(U u,Class v) throws ServiceException{
		V vo=null;
		try {
			Constructor<V> c=v.getConstructor();
			vo=c.newInstance();
			BeanUtils.copyProperties(v, u);
		} catch (IllegalAccessException|InvocationTargetException|NoSuchMethodException|SecurityException|InstantiationException e) {
			throw new ServiceException(e.getMessage());
		} 
		return vo;
	}
	
	public static <U,V> List<V> convertListUToV(List<U> us,Class v) throws ServiceException{
		List<V> vs=new ArrayList<>(us.size());
		for(U u:us) {
			try {
				Constructor<V> c=v.getConstructor();
				V vo=c.newInstance();
				BeanUtils.copyProperties(v, u);
				vs.add(vo);
			} catch (IllegalAccessException|InvocationTargetException|NoSuchMethodException|SecurityException|InstantiationException e) {
				throw new ServiceException(e.getMessage());
			}
		}
		return vs;
	}
}
